namespace HotelControl.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DataModel : DbContext
    {
        public DataModel()
            : base("name=DataModel")
        {
        }

        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<Jobs> Jobs { get; set; }
        public virtual DbSet<RoomCategory> RoomCategory { get; set; }
        public virtual DbSet<Rooms> Rooms { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<UsedServices> UsedServices { get; set; }
        public virtual DbSet<Workers> Workers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Clients>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Clients>()
                .Property(e => e.Surname)
                .IsFixedLength();

            modelBuilder.Entity<Clients>()
                .Property(e => e.Age)
                .IsFixedLength();

            modelBuilder.Entity<Clients>()
                .Property(e => e.Sex)
                .IsFixedLength();

            modelBuilder.Entity<Clients>()
                .HasMany(e => e.UsedServices)
                .WithOptional(e => e.Clients)
                .HasForeignKey(e => e.Client);

            modelBuilder.Entity<Jobs>()
                .Property(e => e.JobName)
                .IsFixedLength();

            modelBuilder.Entity<Jobs>()
                .HasMany(e => e.Workers)
                .WithOptional(e => e.Jobs)
                .HasForeignKey(e => e.Job);

            modelBuilder.Entity<RoomCategory>()
                .Property(e => e.CategoryName)
                .IsFixedLength();

            modelBuilder.Entity<RoomCategory>()
                .HasMany(e => e.Rooms)
                .WithOptional(e => e.RoomCategory)
                .HasForeignKey(e => e.Category);

            modelBuilder.Entity<Rooms>()
                .HasMany(e => e.Clients)
                .WithOptional(e => e.Rooms)
                .HasForeignKey(e => e.Room);

            modelBuilder.Entity<Rooms>()
                .Property(e => e.Status)
                .IsFixedLength();

            modelBuilder.Entity<Services>()
                .Property(e => e.ServiceName)
                .IsFixedLength();

            modelBuilder.Entity<Services>()
                .HasMany(e => e.UsedServices)
                .WithOptional(e => e.Services)
                .HasForeignKey(e => e.Service);

            modelBuilder.Entity<Workers>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Workers>()
                .Property(e => e.Surname)
                .IsFixedLength();

            modelBuilder.Entity<Workers>()
                .Property(e => e.Education)
                .IsFixedLength();

            modelBuilder.Entity<Workers>()
                .Property(e => e.Sex)
                .IsFixedLength();
        }
    }
}
