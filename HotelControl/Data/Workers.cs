namespace HotelControl.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Workers
    {
        [Key]
        public int WorkerId { get; set; }

        [StringLength(15)]
        public string Name { get; set; }

        [StringLength(15)]
        public string Surname { get; set; }

        public int? Job { get; set; }

        [StringLength(10)]
        public string Education { get; set; }

        public int? Age { get; set; }

        [StringLength(3)]
        public string Sex { get; set; }

        public virtual Jobs Jobs { get; set; }
    }
}
