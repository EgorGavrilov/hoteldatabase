namespace HotelControl.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RoomCategory")]
    public partial class RoomCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RoomCategory()
        {
            Rooms = new HashSet<Rooms>();
        }

        [Key]
        public int CategoryId { get; set; }

        [StringLength(15)]
        public string CategoryName { get; set; }

        public int? RoomCount { get; set; }

        public int? GuestCount { get; set; }

        public int? Cost { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rooms> Rooms { get; set; }
    }
}
