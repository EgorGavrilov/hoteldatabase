namespace HotelControl.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UsedServices
    {
        public int Id { get; set; }

        public int? Client { get; set; }

        public int? Service { get; set; }

        public virtual Clients Clients { get; set; }

        public virtual Services Services { get; set; }
    }
}
