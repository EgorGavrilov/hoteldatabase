﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelControl.ViewModel
{
    public class ServiceModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}