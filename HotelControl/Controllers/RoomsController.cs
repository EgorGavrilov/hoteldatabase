﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using HotelControl.Data;

namespace HotelControl.Controllers
{
    public class RoomsController : Controller
    {
        private DataModel db = new DataModel();

        // GET: Rooms
        public ViewResult Index(string categoryName)
        {
            UpdateStatuses();
            List<Rooms> rooms;
            if (!String.IsNullOrEmpty(categoryName))
                rooms = db.Rooms.Where(r => r.RoomCategory.CategoryName.ToUpper().Contains(categoryName.ToUpper()))
                    .Include(r => r.RoomCategory)
                    .ToList();
            else
                rooms = db.Rooms.Include(r => r.RoomCategory).ToList();

            return View(rooms);
        }

        // GET: Rooms/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rooms rooms = db.Rooms.Find(id);
            if (rooms == null)
            {
                return HttpNotFound();
            }
            return View(rooms);
        }

        // GET: Rooms/Create
        public ActionResult Create()
        {
            ViewBag.Category = new SelectList(db.RoomCategory, "CategoryId", "CategoryName");
            return View();
        }

        // POST: Rooms/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RoomId,Number,Category,Status")] Rooms rooms)
        {
            if (ModelState.IsValid)
            {
                db.Rooms.Add(rooms);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Category = new SelectList(db.RoomCategory, "CategoryId", "CategoryName", rooms.Category);
            return View(rooms);
        }

        // GET: Rooms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rooms rooms = db.Rooms.Find(id);
            if (rooms == null)
            {
                return HttpNotFound();
            }
            ViewBag.Category = new SelectList(db.RoomCategory, "CategoryId", "CategoryName", rooms.Category);
            return View(rooms);
        }

        // POST: Rooms/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RoomId,Number,Category,Status")] Rooms rooms)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rooms).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Category = new SelectList(db.RoomCategory, "CategoryId", "CategoryName", rooms.Category);
            return View(rooms);
        }

        // GET: Rooms/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rooms rooms = db.Rooms.Find(id);
            if (rooms == null)
            {
                return HttpNotFound();
            }
            return View(rooms);
        }

        // POST: Rooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rooms rooms = db.Rooms.Find(id);
            db.Rooms.Remove(rooms);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void UpdateStatuses()
        {
            foreach (var room in db.Rooms)
            {
                room.Status = "Свободен";
            }
            foreach (var client in db.Clients)
            {
                int roomID = -1;
                roomID = client.Room ?? client.Room.Value;
                if (roomID != -1)
                {
                    var str = "Занят";
                    if (client.StartDate > DateTime.Now) str = "Забронирован";
                    db.Rooms.Find(roomID).Status = str;
                }
            }
            db.SaveChanges();
        }
    }
}
