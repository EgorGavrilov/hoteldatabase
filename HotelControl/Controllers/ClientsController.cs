﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelControl.Data;

namespace HotelControl.Controllers
{
    public class ClientsController : Controller
    {
        private DataModel db = new DataModel();

        // GET: Clients
        public ViewResult Index(string searchString)
        {
            List<Clients> clients;
            if (!String.IsNullOrEmpty(searchString))
            {
                clients= db.Clients.Where(s => s.Surname.ToUpper().Contains(searchString.ToUpper())
                                       || s.Name.ToUpper().Contains(searchString.ToUpper())).Include(c => c.Rooms).ToList();
            }
            else
                clients = db.Clients.Include(c => c.Rooms).Include(c => c.UsedServices).ToList();

            return View(clients);
        }

        public ViewResult Total(int id)
        {
            Clients client = db.Clients.Find(id);
            var totalPrice = (int)(client.EndDate.Value - client.StartDate.Value).TotalDays *
                                db.Rooms.Find(client.Room).RoomCategory.Cost;
            ViewBag.Living = totalPrice;
            var used = client.UsedServices.GroupBy(x => x.Services.ServiceName).Select(s => new {s.Key, s.ToList().Count});
            
            foreach (var service in client.UsedServices)
            {
                totalPrice += service.Services.Cost;
            }
            ViewBag.Used = used;
            ViewBag.TotalPrice = totalPrice;
            return View(client);
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clients clients = db.Clients.Find(id);
            var services = db.UsedServices.Where(x => x.Client == id);
            if (clients == null)
            {
                return HttpNotFound();
            }
            return View(clients);
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            ViewBag.Room = new SelectList(db.Rooms, "RoomId", "Number");
            return View();
        }

        // POST: Clients/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClientId,Name,Surname,Age,Sex,Room,StartDate,EndDate")] Clients clients)
        {
            if (ModelState.IsValid)
            {
                db.Clients.Add(clients);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Room = new SelectList(db.Rooms, "RoomId", "Number", clients.Room);
            return View(clients);
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clients clients = db.Clients.Find(id);
            if (clients == null)
            {
                return HttpNotFound();
            }
            ViewBag.Room = new SelectList(db.Rooms, "RoomId", "Number", clients.Room);
            ViewBag.Service = new SelectList(db.Services, "ServiceId", "ServiceName", clients.UsedServices);
            return View(clients);
        }

        // POST: Clients/Edit/5
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClientId,Name,Surname,Age,Sex,Room,StartDate,EndDate")] Clients clients)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clients).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Room = new SelectList(db.Rooms, "RoomId", "Number", clients.Room);
            return View(clients);
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clients clients = db.Clients.Find(id);
            if (clients == null)
            {
                return HttpNotFound();
            }
            return View(clients);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Clients clients = db.Clients.Find(id);
            db.Clients.Remove(clients);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
