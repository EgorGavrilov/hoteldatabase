﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelControl.Data;

namespace HotelControl.Controllers
{
    public class RoomCategoriesController : Controller
    {
        private DataModel db = new DataModel();

        // GET: RoomCategories
        public ActionResult Index()
        {
            return View(db.RoomCategory.ToList());
        }

        // GET: RoomCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomCategory roomCategory = db.RoomCategory.Find(id);
            if (roomCategory == null)
            {
                return HttpNotFound();
            }
            return View(roomCategory);
        }

        // GET: RoomCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RoomCategories/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryId,CategoryName,RoomCount,GuestCount,Cost")] RoomCategory roomCategory)
        {
            if (ModelState.IsValid)
            {
                db.RoomCategory.Add(roomCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(roomCategory);
        }

        // GET: RoomCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomCategory roomCategory = db.RoomCategory.Find(id);
            if (roomCategory == null)
            {
                return HttpNotFound();
            }
            return View(roomCategory);
        }

        // POST: RoomCategories/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryId,CategoryName,RoomCount,GuestCount,Cost")] RoomCategory roomCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roomCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roomCategory);
        }

        // GET: RoomCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomCategory roomCategory = db.RoomCategory.Find(id);
            if (roomCategory == null)
            {
                return HttpNotFound();
            }
            return View(roomCategory);
        }

        // POST: RoomCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RoomCategory roomCategory = db.RoomCategory.Find(id);
            db.RoomCategory.Remove(roomCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
