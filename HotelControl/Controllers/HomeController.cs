﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.UI.WebControls;
using HotelControl.Data;
using HotelControl.ViewModel;

namespace HotelControl.Controllers
{
    public class HomeController : Controller
    {
        private DataModel db = new DataModel();

        public ActionResult Index()
        {
            return View();
        }

        public ViewResult FreeNumber(string dateString)
        {
            if (!String.IsNullOrEmpty(dateString))
            {
                var date = DateTime.Parse(dateString);
                var used = db.Clients.Where(c => c.StartDate < date &&
                                             c.EndDate > date).Select(x => x.Rooms).ToList();
                var free = db.Rooms.ToList().Except(used).ToList();
                return View(free);
            }
            var rooms = new List<Rooms>();
            return View(rooms);
        }

        public ActionResult ServicePopularity()
        {
            var used = db.UsedServices
                .GroupBy(x => x.Services.ServiceName)
                .Select(s => new ServiceModel() {Name = s.Key, Count = s.ToList().Count})
                .OrderByDescending(t => t.Count)
                .ToList();
            return View(used);
        }

        public ViewResult ClientForDate(string dateString1, string dateString2)
        {
            if (!String.IsNullOrEmpty(dateString1) && !String.IsNullOrEmpty(dateString2))
            {
                var date1 = DateTime.Parse(dateString1);
                var date2 = DateTime.Parse(dateString2);
                var clients = db.Clients.Where(c => c.StartDate < date1 &&
                                             c.EndDate > date2 || c.StartDate < date2).ToList();
                return View(clients);
            }
            var client = new List<Clients>();
            return View(client);
        }
    }
}