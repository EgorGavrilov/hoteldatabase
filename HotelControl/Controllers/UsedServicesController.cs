﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelControl.Data;

namespace HotelControl.Controllers
{
    public class UsedServicesController : Controller
    {
        private DataModel db = new DataModel();

        // GET: UsedServices
        public ActionResult Index()
        {
            var usedServices = db.UsedServices.Include(u => u.Clients).Include(u => u.Services);
            return View(usedServices.ToList());
        }

        // GET: UsedServices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsedServices usedServices = db.UsedServices.Find(id);
            if (usedServices == null)
            {
                return HttpNotFound();
            }
            return View(usedServices);
        }

        // GET: UsedServices/Create
        public ActionResult Create()
        {
            ViewBag.Client = new SelectList(db.Clients, "ClientId", "Surname");
            ViewBag.Service = new SelectList(db.Services, "ServiceId", "ServiceName");
            return View();
        }

        // POST: UsedServices/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Client,Service")] UsedServices usedServices)
        {
            if (ModelState.IsValid)
            {
                db.UsedServices.Add(usedServices);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Client = new SelectList(db.Clients, "ClientId", "Surname", usedServices.Client);
            ViewBag.Service = new SelectList(db.Services, "ServiceId", "ServiceName", usedServices.Service);
            return View(usedServices);
        }

        // GET: UsedServices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsedServices usedServices = db.UsedServices.Find(id);
            if (usedServices == null)
            {
                return HttpNotFound();
            }
            ViewBag.Client = new SelectList(db.Clients, "ClientId", "Surname", usedServices.Client);
            ViewBag.Service = new SelectList(db.Services, "ServiceId", "ServiceName", usedServices.Service);
            return View(usedServices);
        }

        // POST: UsedServices/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Client,Service")] UsedServices usedServices)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usedServices).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Client = new SelectList(db.Clients, "ClientId", "Surname", usedServices.Client);
            ViewBag.Service = new SelectList(db.Services, "ServiceId", "ServiceName", usedServices.Service);
            return View(usedServices);
        }

        // GET: UsedServices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsedServices usedServices = db.UsedServices.Find(id);
            if (usedServices == null)
            {
                return HttpNotFound();
            }
            return View(usedServices);
        }

        // POST: UsedServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UsedServices usedServices = db.UsedServices.Find(id);
            db.UsedServices.Remove(usedServices);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
